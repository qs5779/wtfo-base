# Before 'make install' is performed this script should be runnable with
# 'make test'. After 'make install' it should work as 'perl Wtfo-Base.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use strict;
use warnings;

use Test::More tests => 5;
#BEGIN { use_ok('Wtfo::Base') };

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

use Wtfo::Base;

my $p = Wtfo::Base->new;
isa_ok($p, 'Wtfo::Base');
ok($p->debug() == 0);
ok($p->verbose() == 0);
ok($p->quiet() == 0);
ok($p->test() == 0);
