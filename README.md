# README #

Over the years I have found myself duplicating far too much code. I have finally decided it is long 
past the time to create some modules.

### What is this repository for? ###

* Repository to house work for my base perl module.

### How do I get set up? ###

* An exercise for the reader.

### Contribution guidelines ###

* Any contributions or comments welcome.

### Who do I talk to? ###

* quiensabe at quiensabe dot org

### INSTALLATION ###
* To install this module type the following:

   ** perl Makefile.PL
   ** make
   ** make test
   ** make install
