package Wtfo::Base;

use 5.016003;
use strict;
use warnings;
use Moose;
use namespace::autoclean;
require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Wtfo::Base ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(

) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
);

our $VERSION = '0.01';


# Preloaded methods go here.

has 'debug' => ( is  => 'rw', isa => 'Int', default => 0 );
has 'verbose' => ( is  => 'rw', isa => 'Int', default => 0 );
has 'test' => ( is  => 'rw', isa => 'Bool', default => 0 );
has 'quiet' => ( is  => 'rw', isa => 'Bool', default => 0 );

__PACKAGE__->meta->make_immutable;

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Wtfo::Base - Perl extension to act as a base for the modules I use, it basically manages
and shared the standard options all or most of my perl scripts use.

=head1 SYNOPSIS

  use Wtfo::Base;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for Wtfo::Base, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

jim, E<lt>jim@E<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2017 by jim

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.16.3 or,
at your option, any later version of Perl 5 you may have available.


=cut
